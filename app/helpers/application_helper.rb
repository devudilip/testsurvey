module ApplicationHelper

  def get_field_tag question
    if question.custom_field.name == "Numeric"
      tag("input", type: 'text', name:  "survey[#{question.id}][]")
    elsif question.custom_field.name == "Essay"
      tag("input", type: 'text_area', name:  "survey[#{question.id}][]")
    elsif question.custom_field.name == "Radio Button"
      radio = ''
      options = question.options.split("\n")
      options.collect{ |text| radio += tag("input", type: 'radio', name:  "survey[#{question.id}][]", value: text)  + text }
      return raw(radio)
    elsif question.custom_field.name == "Date"
      tag("input", type: 'text', name:  "survey[#{question.id}][]")
    elsif  question.custom_field.name == "Check Box" 
      tag("input", type: 'checkbox', name:  "survey[#{question.id}][]", value: 'test')  + "Test"
    end
  end

end

