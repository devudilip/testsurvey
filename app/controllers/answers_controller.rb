class AnswersController < ApplicationController
  def new
    @survey_name = SurveyName.find params[:survey_name_id]
    @answer = @survey_name.answers.new
  end

  def create
    @survey_name = SurveyName.find params[:survey_name_id]
    @answer = @survey_name.answers.new
    survey =  params[:survey]
    @answers = params[:survey].collect { |ans| @survey_name.answers.new(answers: ans.last, user_id: 1, question_id: ans.first) }
    # debugger
    if @answers.all?(&:valid?)
      @answers.each(&:save!)
      redirect_to survey_names_path
    else
      debugger
      render :action => 'new'
    end
  end
end
