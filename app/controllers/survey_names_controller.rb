class SurveyNamesController < ApplicationController
  # GET /survey_names
  # GET /survey_names.json
  def index
    @survey_names = SurveyName.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @survey_names }
    end
  end

  # GET /survey_names/1
  # GET /survey_names/1.json
  def show
    @survey_name = SurveyName.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @survey_name }
    end
  end

  # GET /survey_names/new
  # GET /survey_names/new.json
  def new
    @survey_name = SurveyName.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @survey_name }
    end
  end

  # GET /survey_names/1/edit
  def edit
    @survey_name = SurveyName.find(params[:id])
  end

  # POST /survey_names
  # POST /survey_names.json
  def create
    @survey_name = SurveyName.new(params[:survey_name])

    respond_to do |format|
      if @survey_name.save
        format.html { redirect_to @survey_name, notice: 'Survey name was successfully created.' }
        format.json { render json: @survey_name, status: :created, location: @survey_name }
      else
        format.html { render action: "new" }
        format.json { render json: @survey_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /survey_names/1
  # PUT /survey_names/1.json
  def update
    @survey_name = SurveyName.find(params[:id])

    respond_to do |format|
      if @survey_name.update_attributes(params[:survey_name])
        format.html { redirect_to @survey_name, notice: 'Survey name was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @survey_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /survey_names/1
  # DELETE /survey_names/1.json
  def destroy
    @survey_name = SurveyName.find(params[:id])
    @survey_name.destroy

    respond_to do |format|
      format.html { redirect_to survey_names_url }
      format.json { head :no_content }
    end
  end
end
