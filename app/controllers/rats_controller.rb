class RatsController < ApplicationController
  # GET /rats
  # GET /rats.json
  def index
    @rats = Rat.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @rats }
    end
  end

  # GET /rats/1
  # GET /rats/1.json
  def show
    @rat = Rat.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @rat }
    end
  end

  # GET /rats/new
  # GET /rats/new.json
  def new
    @rat = Rat.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @rat }
    end
  end

  # GET /rats/1/edit
  def edit
    @rat = Rat.find(params[:id])
  end

  # POST /rats
  # POST /rats.json
  def create
    @rat = Rat.new(params[:rat])

    respond_to do |format|
      if @rat.save
        format.html { redirect_to @rat, notice: 'Rat was successfully created.' }
        format.json { render json: @rat, status: :created, location: @rat }
      else
        format.html { render action: "new" }
        format.json { render json: @rat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /rats/1
  # PUT /rats/1.json
  def update
    @rat = Rat.find(params[:id])

    respond_to do |format|
      if @rat.update_attributes(params[:rat])
        format.html { redirect_to @rat, notice: 'Rat was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @rat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rats/1
  # DELETE /rats/1.json
  def destroy
    @rat = Rat.find(params[:id])
    @rat.destroy

    respond_to do |format|
      format.html { redirect_to rats_url }
      format.json { head :no_content }
    end
  end
end
