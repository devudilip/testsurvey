class QuestionsController < ApplicationController


  def index
    @survey_name = SurveyName.find params[:survey_name_id]
    @questions = @survey_name.questions
  end

  def new
    @survey_name = SurveyName.find params[:survey_name_id]
    @question = @survey_name.questions.new
  end
  def create
    p params
    @survey_name = SurveyName.find params[:survey_name_id]
    @question = @survey_name.questions.new(params[:question])
    if @question.save
      redirect_to survey_names_path
    else
      render 'new'
    end
  end

  def edit
    @survey_name = SurveyName.find params[:survey_name_id]
    @question = @survey_name.questions.find params[:id]
  end

  def update
    @survey_name = SurveyName.find params[:survey_name_id]
    @question = @survey_name.questions.find params[:id]
    if @question.update_attributes params[:question]
      redirect_to survey_name_path @survey_name
    else
      render 'edit'
    end
  end
end
