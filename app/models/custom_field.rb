class CustomField < ActiveRecord::Base
  attr_accessible :name
  has_many :questions, dependent: :destroy

  def essay
    "options can't be blank for this type"
  end

  def numeric

  end
  def radiobutton
    false
  end
  def date

  end
  def checkbox
  end
end
