# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :rat do
    name "MyString"
    phone 1
    sex false
    address "MyText"
  end
end
