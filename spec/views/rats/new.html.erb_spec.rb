require 'rails_helper'

RSpec.describe "rats/new", :type => :view do
  before(:each) do
    assign(:rat, Rat.new(
      :name => "MyString",
      :phone => 1,
      :sex => false,
      :address => "MyText"
    ))
  end

  it "renders new rat form" do
    render

    assert_select "form[action=?][method=?]", rats_path, "post" do

      assert_select "input#rat_name[name=?]", "rat[name]"

      assert_select "input#rat_phone[name=?]", "rat[phone]"

      assert_select "input#rat_sex[name=?]", "rat[sex]"

      assert_select "textarea#rat_address[name=?]", "rat[address]"
    end
  end
end
