require 'rails_helper'

RSpec.describe "rats/show", :type => :view do
  before(:each) do
    @rat = assign(:rat, Rat.create!(
      :name => "Name",
      :phone => 1,
      :sex => false,
      :address => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/MyText/)
  end
end
