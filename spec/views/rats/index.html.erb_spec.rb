require 'rails_helper'

RSpec.describe "rats/index", :type => :view do
  before(:each) do
    assign(:rats, [
      Rat.create!(
        :name => "Name",
        :phone => 1,
        :sex => false,
        :address => "MyText"
      ),
      Rat.create!(
        :name => "Name",
        :phone => 1,
        :sex => false,
        :address => "MyText"
      )
    ])
  end

  it "renders a list of rats" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
