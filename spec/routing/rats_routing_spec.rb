require "rails_helper"

RSpec.describe RatsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/rats").to route_to("rats#index")
    end

    it "routes to #new" do
      expect(:get => "/rats/new").to route_to("rats#new")
    end

    it "routes to #show" do
      expect(:get => "/rats/1").to route_to("rats#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/rats/1/edit").to route_to("rats#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/rats").to route_to("rats#create")
    end

    it "routes to #update" do
      expect(:put => "/rats/1").to route_to("rats#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/rats/1").to route_to("rats#destroy", :id => "1")
    end

  end
end
