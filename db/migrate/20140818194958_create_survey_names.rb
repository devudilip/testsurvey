class CreateSurveyNames < ActiveRecord::Migration
  def change
    create_table :survey_names do |t|
      t.string :name

      t.timestamps
    end
  end
end
