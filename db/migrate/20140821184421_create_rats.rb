class CreateRats < ActiveRecord::Migration
  def change
    create_table :rats do |t|
      t.string :name
      t.integer :phone
      t.boolean :sex
      t.text :address

      t.timestamps
    end
  end
end
