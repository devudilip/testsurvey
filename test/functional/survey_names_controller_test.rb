require 'test_helper'

class SurveyNamesControllerTest < ActionController::TestCase
  setup do
    @survey_name = survey_names(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:survey_names)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create survey_name" do
    assert_difference('SurveyName.count') do
      post :create, survey_name: { name: @survey_name.name }
    end

    assert_redirected_to survey_name_path(assigns(:survey_name))
  end

  test "should show survey_name" do
    get :show, id: @survey_name
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @survey_name
    assert_response :success
  end

  test "should update survey_name" do
    put :update, id: @survey_name, survey_name: { name: @survey_name.name }
    assert_redirected_to survey_name_path(assigns(:survey_name))
  end

  test "should destroy survey_name" do
    assert_difference('SurveyName.count', -1) do
      delete :destroy, id: @survey_name
    end

    assert_redirected_to survey_names_path
  end
end
